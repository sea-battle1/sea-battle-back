FROM node

WORKDIR /sea-battles

COPY . .

RUN npm install

RUN npm run build

EXPOSE 4400

CMD ["node", "dist/index.js"]


