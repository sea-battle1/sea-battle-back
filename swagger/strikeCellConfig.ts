export const strikeCellConfig = {
    "/strikeCell" : {
        get: {
            tags: ['Single Game'],
            produces: ["application/json"],
            parameters: [{
                in: 'query',
                name: 'gameId',
                type: 'string',
                required: true,
                description: 'Game identificator'
            }, {
                in: 'query',
                name: 'row',
                type: 'string',
                required: true,
                description: 'Row index'
            }, {
                in: 'query',
                name: 'col',
                type: 'string',
                required: true,
                description: 'Column index'
            }],
            "responses": {
                "200": {
                    content: {
                        'application/json': {
                            "schema": {
                                "type": "object",
                                "required": ["userId", 'userField'],
                                "properties": {
                                    playerField: {
                                        type: 'array',
                                        minItems: 10,
                                        required: true,
                                        items: {
                                            type: 'array',
                                            items: {
                                                type: 'integer',
                                            },
                                            minItems: 10,
                                        },
                                    },
                                    enemyField: {
                                        type: 'array',
                                        minItems: 10,
                                        required: true,
                                        items: {
                                            type: 'array',
                                            items: {
                                                type: 'integer',
                                            },
                                            minItems: 10,
                                        },
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
