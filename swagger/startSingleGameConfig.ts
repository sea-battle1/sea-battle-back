export const startSingleGameConfig = {
    "/startSingleGame": {
        post: {
            tags: ['Single Game'],
            produces: ["application/json"],
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            "required": ["userId", 'userField'],
                            "properties": {
                                "userId": {
                                    "type": "string"
                                },
                                userField: {
                                    type: 'array',
                                    minItems: 10,
                                    items: {
                                        type: 'array',
                                        items: {
                                            type: 'integer',
                                        },
                                        minItems: 10,
                                    }
                                }
                            }
                        }
                    }
                },
            },
            "responses": {
                "200": {
                    content: {
                        'application/json': {
                            "schema": {
                                "type": "object",
                                "required": ["userId", 'userField'],
                                "properties": {
                                    "gameId": {
                                        "type": "string",
                                        required: true,
                                    },
                                    enemyField: {
                                        type: 'array',
                                        minItems: 10,
                                        required: true,
                                        items: {
                                            type: 'array',
                                            items: {
                                                type: 'integer',
                                            },
                                            minItems: 10,
                                        },
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
