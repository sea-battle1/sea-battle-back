import swaggerJSDoc from "swagger-jsdoc";
import {strikeCellConfig} from "./strikeCellConfig";
import {startSingleGameConfig} from "./startSingleGameConfig";

export const options: swaggerJSDoc.Options = {
    failOnErrors: true,
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Sea battle Backend',
            version: '1.0.0',
        },
        tags: [{name: 'Single Game'}],
        paths: {
        ...startSingleGameConfig,
        ...strikeCellConfig,
        }
    },
    apis: ['dist/src/routes/*.js'],
};
