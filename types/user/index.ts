import {CreationOptional, InferAttributes, InferCreationAttributes, Model} from "sequelize";
import {AuthTypes} from "../enums";

export interface UserModelI extends Model<InferAttributes<UserModelI>, InferCreationAttributes<UserModelI>> {
    userId: string,
    first_name: string,
    last_name: string,
    nick_name: string,
    authType: AuthTypes,
    singleGames: number,
    singleWin: number,
    singleLoose: number,
    simulateGames: number,
    simulateWin: number,
    simulateLoose: number,
    mpGames: number,
    mpWin: number,
    mpLoose: number,
    allWin: CreationOptional<number>,
    allGames: CreationOptional<number>,
    relativePercent: CreationOptional<number>
}