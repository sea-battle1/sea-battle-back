import {InferAttributes, InferCreationAttributes, Model} from "sequelize";
import {FieldType} from "../enums";

export enum GameRecordTypes {
    single = 'single',
    mp = 'mp'
}

export type StrikeType = {
    row: number,
    column: number,
    timeFromStart: number
    isEndGame?: boolean
}

export interface GameRecordModelI extends Model<InferAttributes<GameRecordModelI>, InferCreationAttributes<GameRecordModelI>> {
    gameId: string,
    type: GameRecordTypes,
    creatorPlayerId: string,
    secondPlayerId: string,
    gameStartTime: string,
    gameEndTime: string,
    startCreatorPlayerField: FieldType,
    startSecondPlayerField: FieldType,
    creatorPlayerStrikes: StrikeType[],
    secondPlayerStrikes: StrikeType[],
    isEndTimeout: boolean,
    winnerId: string
}

export type GameDataInRedisType = {
    creatorPlayerCurrentField: FieldType,
    secondPlayerCurrentField: FieldType,
    type: GameRecordTypes,
    gameId: string,
    creatorPlayerId: string,
    secondPlayerId: string,
    gameStartTime: number,
    gameEndTime: number,
    startCreatorPlayerField: FieldType,
    startSecondPlayerField: FieldType,
    creatorPlayerStrikes: StrikeType[],
    secondPlayerStrikes: StrikeType[]
}
