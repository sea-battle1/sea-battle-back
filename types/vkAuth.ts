import {AuthTypes} from "./enums";

type VKAuthStatusType = "connected" | 'not_authorized' |  'unknown'

export type SessionVKType = {
    "mid": string,
    "sid": string,
    "sig": string,
    "secret": string,
    "expire": number
}

export type VKAuthGetLoginStatus = {
    session: SessionVKType,
    status: VKAuthStatusType
}

export type UserCreateTypes = {
    userId: string,
    email?: string,
    first_name: string,
    last_name: string,
    nick_name?: string,
    authType: AuthTypes,
}
