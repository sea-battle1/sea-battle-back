export enum MPEventsTypes {
    startGame = 'startGame',
    strike = 'strike',
    endGame = 'endGame',
    move = 'move',
    mouseleave = 'mouseleave',
    mouseenter = 'mouseenter',
    error = 'error',
}

export type WS_MP_Message_Types = {
    type: MPEventsTypes,
    userId: string,
    userField: number[][]
}

export type WS_MP_Strike_Types = {
    type: MPEventsTypes,
    gameId: string,
    userId: string,
    enemyId: string,
    rowIndex: number,
    columnIndex: number,
}

export type WS_MP_Move_Types = {
    type: MPEventsTypes.move | MPEventsTypes.mouseenter | MPEventsTypes.mouseleave,
    gameId: string,
    enemyId: string,
    fieldY: number,
    fieldX: number,
}
