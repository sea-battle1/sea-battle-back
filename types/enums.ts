export enum CellValueEnum {
    empty = 0,
    miss = 1,
    ship = 2,
    kill = 3,
    support = 4
}

export type FieldType = CellValueEnum[][]

export enum AuthTypes {
    VK = 'VK',
    VKGame = 'VKGame',
    NA6 = 'NA6',
    anotherTypes = 'anotherTypes',
    Google = 'Google',
    Apple = 'Apple',
    TG = 'TG'
}
