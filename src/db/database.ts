import {Dialect, Sequelize} from 'sequelize'

const sequelize = new Sequelize(process.env.DB_NAME_SEA_BATTLE as string, process.env.DB_USERNAME as string, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_TYPE as Dialect,
    port: +(process.env.DB_PORT as string),
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: false
})

export default sequelize
