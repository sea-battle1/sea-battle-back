import {DataTypes, FindOptions} from "sequelize";
import sequelize from './database';
import {UserCreateTypes} from "../../types/vkAuth";
import {UserModelI} from "../../types/user";

export const user = sequelize.define<UserModelI>('user', {
    userId: {
        primaryKey: true,
        allowNull: false,
        type: DataTypes.STRING
    },
    first_name: {
        allowNull: false,
        type: DataTypes.STRING
    },
    last_name: {
        allowNull: false,
        type: DataTypes.STRING
    },
    nick_name: {
        allowNull: true,
        type: DataTypes.STRING
    },
    authType: {
        allowNull: false,
        type: DataTypes.STRING
    },
    singleGames: {
        allowNull: false,
        type: DataTypes.INTEGER,
    },
    singleWin: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    singleLoose: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    simulateGames: {
        allowNull: false,
        type: DataTypes.INTEGER,
    },
    simulateWin: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    simulateLoose: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    mpGames: {
        allowNull: false,
        type: DataTypes.INTEGER,

    },
    mpWin: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    mpLoose: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    allWin: {
        type: DataTypes.VIRTUAL,
        get() {
            // @ts-ignore
            return this.mpWin + this.simulateWin + this.singleWin
        }
    },
    allGames: {
        type: DataTypes.VIRTUAL,
        get() {
            return this.mpGames + this.simulateGames + this.singleGames
        }
    },
    relativePercent: {
        type: DataTypes.VIRTUAL,
        get() {
            return +((this.allWin / this.allGames) * 100 || 0).toFixed(1)
        }
    }
})

export const findUserById = async (id: string): Promise<UserCreateTypes | null> => {
    const userData = await user.findOne({where: {userId: id}} as any)
    if (!userData) return null

    return {
        userId: userData.getDataValue('userId'),
        first_name: userData.getDataValue('first_name'),
        last_name: userData.getDataValue('last_name'),
        nick_name: userData.getDataValue('nick_name'),
        authType: userData.getDataValue('authType'),
    }
}

export const getUserInfoById = async (id: string) => {
    const userData = await user.findOne({where: {userId: id}} as any)
    if (!userData) return null
    return userData
}

export const getUsersList = async (options?: FindOptions) => {
    return await user.findAll(options)
}

export const createUser = async ({userId, first_name, last_name, nick_name, authType}: UserCreateTypes) => {
    await user.create({
        userId,
        first_name,
        last_name,
        nick_name: nick_name || '',
        authType,
        singleLoose: 0,
        singleWin: 0,
        singleGames: 0,
        simulateLoose: 0,
        simulateWin: 0,
        simulateGames: 0,
        mpLoose: 0,
        mpWin: 0,
        mpGames: 0
    })
    return {
        userId,
        first_name,
        last_name,
        nick_name,
        authType,
    }
}

export const increaseSingleGamesCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('singleGames')
}

export const increaseSingleWinsCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('singleWin')
}

export const increaseSingleLooseCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('singleLoose')
}

export const increaseSimulateGamesCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('simulateGames')
}

export const increaseSimulateWinsCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('simulateWin')
}

export const increaseSimulateLooseCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('simulateLoose')
}

export const increaseMpGamesCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('mpGames')
}

export const increaseMpWinsCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('mpWin')
}

export const increaseMpLooseCount = async (userId: string) => {
    const userData = await user.findOne({where: {userId: userId}} as any)
    if (!userData) return null
    await userData.increment('mpLoose')
}
