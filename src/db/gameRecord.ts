import {DataTypes, Op} from "sequelize";
import * as Sentry from "@sentry/node";
import sequelize from './database';
import {GameRecordModelI, StrikeType} from "../../types/gameRecord";
import {FieldType} from "../../types/enums";

export const gameRecord = sequelize.define<GameRecordModelI>('gameRecord', {
    gameId: {
        primaryKey: true,
        allowNull: false,
        type: DataTypes.STRING
    },
    type: {
        allowNull: false,
        type: DataTypes.STRING
    },
    creatorPlayerId: {
        allowNull: false,
        type: DataTypes.STRING
    },
    secondPlayerId: {
        allowNull: true,
        type: DataTypes.STRING
    },
    gameStartTime: {
        allowNull: false,
        type: DataTypes.DATE
    },
    gameEndTime: {
        allowNull: false,
        type: DataTypes.DATE,
    },
    startCreatorPlayerField: {
        allowNull: false,
        type: DataTypes.JSON
    },
    startSecondPlayerField: {
        allowNull: false,
        type: DataTypes.JSON
    },
    creatorPlayerStrikes: {
        allowNull: false,
        type: DataTypes.JSON,
    },
    secondPlayerStrikes: {
        allowNull: false,
        type: DataTypes.JSON
    },
    isEndTimeout: {
        allowNull: false,
        type: DataTypes.BOOLEAN
    },
    winnerId: {
        allowNull: false,
        type: DataTypes.STRING
    },
}, {
    indexes: [
        {
            fields: ['creatorPlayerId'],
            using: 'HASH',
        },
        {
            fields: ['secondPlayerId'],
            using: 'HASH',
        }
    ]
})

type CreateGameRecordArgType = Pick<GameRecordModelI,
    'gameId' | 'type' | 'creatorPlayerId' | 'secondPlayerId' | 'isEndTimeout' | 'winnerId'> & {
    gameStartTime: number,
    startSecondPlayerField: FieldType,
    startCreatorPlayerField: FieldType,
    creatorPlayerStrikes: StrikeType[],
    secondPlayerStrikes: StrikeType[],
}

export const createGameRecord = async (gameRecordInstance: CreateGameRecordArgType) => {
    try {
        await gameRecord.create({
            ...gameRecordInstance,
            gameStartTime: new Date(gameRecordInstance.gameStartTime).toUTCString(),
            gameEndTime: new Date().toUTCString(),
            startSecondPlayerField: gameRecordInstance.startSecondPlayerField,
            startCreatorPlayerField: gameRecordInstance.startCreatorPlayerField,
            creatorPlayerStrikes: gameRecordInstance.creatorPlayerStrikes,
            secondPlayerStrikes: gameRecordInstance.secondPlayerStrikes,
        })
    } catch (err) {
        console.log(err)
        Sentry.captureException('Не удалось сохранить запись игры');
    }
}

export const getGameRecord = async (gameRecordId: string) => {
    try {
        return await gameRecord.findOne({where: {gameId: gameRecordId}})
    } catch (err) {
        Sentry.captureException('Не удалось получить запись игры');
    }
}

export const getGameRecordsByUserId = async (userId: string) => {
    try {
        return await gameRecord.findAll({
            where: {
                [Op.or]: [
                    {creatorPlayerId: userId},
                    {secondPlayerId: userId}
                ]
            }
        })
    } catch (err) {
        Sentry.captureException('Не удалось получить записи игры');
    }
}

export const getGameCountByDay = async () => {
    try {
        const dayNow = new Date()

        const dayStartTime = Date.now() - (dayNow.getHours() * 60 * 60 *1000 + dayNow.getMinutes() * 60 * 1000 + dayNow.getSeconds() * 1000)
        const dayEndTime = dayStartTime + (24 * 60 * 60 *1000)

        return await gameRecord.count({
            where: {
                gameStartTime: {
                    [Op.between]: [dayStartTime, dayEndTime],
                }
            }
        })
    } catch (err) {
        Sentry.captureException('Не удалось получить количество игр');
    }
}

type GetGameRecordsOptionsType = {
    page: number,
}

export const getGameRecords = async (options: GetGameRecordsOptionsType) => {
    try {
        const count = await gameRecord.count()
        const records = await gameRecord.findAll({
            limit: 20,
            offset: 20 * (options.page - 1),
            order: [
                ['gameStartTime', 'DESC'],
            ]
        })
        return {
            records: records,
            count
        }
    } catch (err) {
        Sentry.captureException('Не удалось получить записи игры');
    }
}
