import {Client} from "redis-om";
import {GameDataInRedisType} from "../../types/gameRecord";

class RedisClient {
    client: Client | null = null

    constructor () {
        new Client().open(process.env.REDIS_URL).then(res => {
            this.client = res
        })
    }

    async setGame (gameId: string, gameValue: GameDataInRedisType) {
        return this.client?.set(gameId, JSON.stringify(gameValue));
    }

    async getGame (gameId: string):Promise<GameDataInRedisType> {
        return this.client?.get(gameId).then(res => res && JSON.parse(res))
    }

    async removeGame (gameId: string) {
        return this.client?.execute(['DEL', gameId])
    }

    async checkIsHaveListWaitingStartGame () {
        const res = await this.client?.execute(['LLEN', 'waitList'])
        if (res === 0) return false
        return true
    }

    async setGameInWaitingList (gameId: string) {
        const res = await this.client?.execute(['RPUSH', 'waitList', gameId])
        if (res === 0) return false
        return true
    }

    async getGameIdFromWaitingList ():Promise<string> {
        return this.client?.execute(['LPOP', 'waitList']) as Promise<string>
    }
}

export const redisClient = new RedisClient()

class GamesDb {
    constructor() {
    }

    setGameInWaitingList (gameId: string) {
        // @ts-ignore
        if (global.waitingList) {
            // @ts-ignore
            global.waitingList.push(gameId)
        } else {
            // @ts-ignore
            global.waitingList = [gameId]
        }
    }
    checkIsHaveListWaitingStartGame () {
        // @ts-ignore
        if (global.waitingList && global.waitingList.length > 0) {
            return true
        }
        return false
    }

    getGameIdFromWaitingList () {
        // @ts-ignore
        return global.waitingList.pop()
    }

    setGame (gameId: string, gameData: any) {
        // @ts-ignore
        global.games[gameId] = {...global.games[gameId], ...gameData}
    }

    getGame (gameId: string) {
        // @ts-ignore
        return global.games[gameId]
    }

    removeGame (gameId: string) {
        // @ts-ignore
        delete global.games[gameId]
    }

    removeBet (betId: string)  {
        // @ts-ignore
        delete global.bets[betId]
    }

    setBet (betId: string, value: any)  {
        // @ts-ignore
        global.bets[betId] = value
    }

    getBet (betId: string)  {
        // @ts-ignore
        return global.bets[betId]
    }
}

export const DB_Client = new GamesDb()
