type ArgsType = {
    [arg: string]: true | string
}
export const getAppArgs = (): ArgsType => {
    const argsArr = process.argv.slice(2)
    const args: any = {}
    argsArr.forEach(el => {
        const prepareEl = el.split('=')
        if (prepareEl.length === 2) {
            args[prepareEl[0]] = prepareEl[1]
        }
        if (prepareEl.length === 1) {
            args[prepareEl[0]] = true
        }
    })
    return args
}