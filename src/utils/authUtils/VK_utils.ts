import {SessionVKType} from "../../../types/vkAuth";
// @ts-ignore
import md5 from 'md5';

export const getVKID = (id:string) => `vk_${id}`

// Функция получения sig при авторизации ВК для проверки подлинности
export const checkSigVK = (session: SessionVKType) => {
    const sig = `expire=${session.expire}mid=${session.mid}secret=${session.secret}sid=${session.sid}${process.env.SECURITY_VK_KEY}`
    return md5(sig) === session.sig
}
