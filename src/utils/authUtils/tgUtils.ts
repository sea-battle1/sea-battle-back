import {AuthTypes} from "../../../types/enums";

export const getTGId = (id:string | number) => `${AuthTypes.TG}_${id}`