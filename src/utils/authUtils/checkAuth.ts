import {Request, Response} from "express";
import {checkBearerToken} from "./AuthUtils";
import * as Sentry from "@sentry/node";

export const authMiddleware = (
    req: Request,
    res: Response
) => {
    if (!req.headers.authorization) {
        res.status(401).json({message: 'Нет токена авторизации'})
        res.end()
        Sentry.captureException('Нет токена авторизации');
        return false
    }

    const isValidToken = checkBearerToken(req.headers.authorization as string)

    if (!isValidToken) {
        Sentry.captureException('Нет токена авторизации');
        res.status(401).json({message: 'Токен авторизации не валиден'})
        res.end()
        return false
    }

    return true
};
