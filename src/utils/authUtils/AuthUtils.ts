// @ts-ignore
import jwt from 'jsonwebtoken';
import * as Sentry from "@sentry/node";

export const getBearerToken = (id:string, first_name:string, last_name:string, sessionExpire:number) => {
    try {
        return jwt.sign({
            userId: id,
            first_name: first_name,
            last_name: last_name,
            sessionExpire: sessionExpire,
        }, process.env.JWT_SECRET_KEY as string);
    } catch (err) {
        Sentry.captureException(err);
    }

}

export const checkBearerToken = (token: string) => {
    try {
        const parseData = jwt.verify(token, process.env.JWT_SECRET_KEY as string)
        // @ts-ignore
        return parseData.sessionExpire > Math.floor(Date.now()/1000)
    } catch (err) {
        Sentry.captureException(err);
        return false
    }
}

export const getUserIdFromToken = (token: string) => {
    try {
        const parseData = jwt.verify(token, process.env.JWT_SECRET_KEY as string)
        return parseData.userId
    } catch (err) {
        Sentry.captureException(err);
    }
}
