import {increaseSingleLooseCount} from "../../db/user";
import {createGameRecord} from "../../db/gameRecord";
import {redisClient} from "../../db/redis";
import {AppConfig} from "../../../appConfig";
import {GameDataInRedisType} from "../../../types/gameRecord";

declare var global: typeof globalThis & {
    timeouts: {
        [gameId: string]:  NodeJS.Timeout
    }
}

export const setTimeoutGameOverSingleGame = (gameId: string, userId: string, gameInstance: GameDataInRedisType) => {
    global.timeouts[gameId] = setTimeout(async () => {
        if (userId) {
            await increaseSingleLooseCount(userId)
        }

        await createGameRecord({
            ...gameInstance,
            isEndTimeout: true,
            winnerId: 'computer'
        })

        await redisClient.removeGame(gameId)

        delete global.timeouts[gameId]
    }, AppConfig.singleGame.interval)
}

export const setTimeoutGameOverMultiplayerGame = (looserId: string, winnerId:string, gameInstance: GameDataInRedisType, cbOnTimeout: () => void) => {
    global.timeouts[gameInstance.gameId] = setTimeout(async () => {
        if (looserId) {
            await increaseSingleLooseCount(looserId)
        }

        await createGameRecord({
            ...gameInstance,
            isEndTimeout: true,
            winnerId: winnerId
        })

        await redisClient.removeGame(gameInstance.gameId)

        delete global.timeouts[gameInstance.gameId]

        cbOnTimeout()
    }, AppConfig.singleGame.interval)
}
