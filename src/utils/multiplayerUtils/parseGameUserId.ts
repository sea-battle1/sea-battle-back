export const parseGameUserId = (id: string): {gameId: string, userId: string} => {
    const [gameId, userId] = id.split(':')
    return {gameId, userId}
}
