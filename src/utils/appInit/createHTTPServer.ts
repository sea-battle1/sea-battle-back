import http from "http";

export const createHTTPServer = (): http.Server => {
    return http.createServer(function (req: any, res: any) {
        res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
        res.end();
    })
}