import * as Sentry from "@sentry/node";
import {Server} from "socket.io";
import {startGame} from "../../routes/multiplayerGameRoutes/helpers/startGame";
import {mpStrikeCell} from "../../routes/multiplayerGameRoutes/helpers/mpStrikeCell";
import {redisClient} from "../../db/redis";
import {getSocketByUserId} from "../../routes/multiplayerGameRoutes/helpers/getSocketByUserId";
import {createGameRecord} from "../../db/gameRecord";
import {increaseMpLooseCount, increaseMpWinsCount} from "../../db/user";
import {MPEventsTypes} from "../../../types/mpTypes";
import {leaveWaitingList} from "../../routes/multiplayerGameRoutes/helpers/leaveWaitingList";

declare var global: typeof globalThis & {
    timeouts: {
        [gameId: string]:  NodeJS.Timeout
    }
}

export const webSocketInit = (io: Server) => {
    io.on("connection",  (socket) => {
        socket.on("startGame", async (arg) => {
            socket.data.userId = arg.userId

            await startGame(arg, io, socket)
        });

        socket.on("strike", async (arg) => {
            await mpStrikeCell(arg, io, socket)
        });

        socket.on("leaveWaitingList", async (arg) => {
            await leaveWaitingList(socket)
        })

        socket.on('playersCount', async () => {
            const sockets = await io.fetchSockets();
            socket.emit('playersCount', sockets.length)
        })

        socket.on('disconnect', async (reason) => {
            try {
                const gameId = socket.data.gameId

                clearTimeout(global.timeouts[gameId])
                const waitListIsHaveGame = await redisClient.checkIsHaveListWaitingStartGame()
                if (waitListIsHaveGame) {
                    const gameIdFromList = await redisClient.getGameIdFromWaitingList()
                    if (gameId === gameIdFromList) {
                        await redisClient.removeGame(gameId)
                    } else {
                        await redisClient.setGameInWaitingList(gameIdFromList)
                    }
                }
                if (gameId) {
                    const gameInstance = await redisClient.getGame(gameId)
                    if (gameInstance) {
                        const userId = socket.data.userId
                        const userIsCreatorGame = userId === gameInstance.creatorPlayerId
                        const enemyId = userIsCreatorGame ? gameInstance.secondPlayerId : gameInstance.creatorPlayerId
                        const enemySocket = await getSocketByUserId(io, enemyId)

                        await createGameRecord({
                            ...gameInstance,
                            isEndTimeout: false,
                            winnerId: enemyId
                        })

                        await increaseMpLooseCount(userId)
                        await increaseMpWinsCount(enemyId)

                        enemySocket?.emit('endGame', {
                            type: MPEventsTypes.endGame,
                            gameId,
                            enemyId: userId,
                            playerField: userIsCreatorGame ? gameInstance.secondPlayerCurrentField : gameInstance.creatorPlayerCurrentField,
                            enemyField: userIsCreatorGame ? gameInstance.creatorPlayerCurrentField : gameInstance.secondPlayerCurrentField,
                            isYouStep: false,
                            result: 1
                        })
                        if (enemySocket) {
                            enemySocket.data.gameId = undefined
                        }
                        await redisClient.removeGame(gameId)
                    }
                }
            } catch (err) {
                Sentry.captureException(err)
            }
        })
    });
}
