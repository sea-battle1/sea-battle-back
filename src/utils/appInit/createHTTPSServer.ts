import https from "https";
import http from "http";
import {Express} from "express";

export const createServer = (app: Express): https.Server | http.Server | undefined => {
    return http.createServer({}, app)
}
