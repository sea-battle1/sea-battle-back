import {Express} from "express";
import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";

// Подключение сентри
export const sentryInit = (app: Express, port: number) => {
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        environment: port === 3004 ? 'production' : 'development',
        release: port === 3004 ? process.env.SENTRY_RELEASE : 'development',
        integrations: [
            new Sentry.Integrations.Http({ tracing: true }),
            new Tracing.Integrations.Express({ app }),
        ],
        tracesSampleRate: 1.0,
    });

    app.use(Sentry.Handlers.requestHandler());
    app.use(Sentry.Handlers.tracingHandler());
}

