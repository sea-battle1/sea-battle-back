import * as Sentry from "@sentry/node";
import {getTGId} from "../authUtils/tgUtils";
import {createUser, findUserById} from "../../db/user";
import {AuthTypes} from "../../../types/enums";
import TelegramBot from "node-telegram-bot-api";

export const tgBotInit = (port: number) => {
    if (port !== 3004) return null

    const tgBot = new TelegramBot(process.env.TG_BOT_TOKEN as string, {
        polling: true,
    })

    tgBot.onText(/help/, (msg:any) => {
        tgBot.sendMessage(msg.from.id, "This bot implements a T-Rex jumping game. Say /game if you want to play.")
    });
    // @ts-ignore
    tgBot.onText(/start|game/, (msg: TelegramBot.Message, match: (RegExpExecArray | null)) => tgBot.sendGame(msg.from.id, process.env.TG_GAME_NAME));

    tgBot.on("callback_query", async (query: TelegramBot.CallbackQuery) => {
        try {
            const copyQuery: TelegramBot.CallbackQuery = JSON.parse(JSON.stringify(query))

            if (copyQuery.game_short_name !== process.env.TG_GAME_NAME) {
                // @ts-ignore
                await tgBot.answerCallbackQuery(copyQuery.id, "Sorry, '" + copyQuery.game_short_name + "' is not available.");
            } else {
                const tgId = getTGId(copyQuery.from.id)
                const userInDb = await findUserById(tgId)
                if (!userInDb) {
                    await createUser({
                        userId: tgId,
                        first_name: copyQuery.from.first_name || 'none',
                        last_name: copyQuery.from.last_name || 'none',
                        nick_name: copyQuery.from.username || 'none',
                        authType: AuthTypes.TG
                    })
                }
                let gameurl = `${process.env.APP_URL_SEA_BATTLE}tgGame/${copyQuery.from.id}/${copyQuery.inline_message_id || `${copyQuery.message?.chat?.id}---${copyQuery.message?.message_id}`}`
                await tgBot.answerCallbackQuery({
                    callback_query_id: copyQuery.id,
                    url: gameurl,
                })
            }
        } catch (err) {
            Sentry.captureException(err);
        }

    })
    tgBot.on("inline_query", async function (iq:any) {
        await tgBot.answerInlineQuery(iq.id, [{
            type: "game",
            id: "0",
            game_short_name: process.env.TG_GAME_NAME as string
        }]);
    });

    return tgBot
}
