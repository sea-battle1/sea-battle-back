import {Express} from "express";

const compression = require('compression')

function shouldCompress (req:any, res:any) {
    if (req.headers['x-no-compression']) {
        return false
    }
    return compression.filter(req, res)
}

export const compressionResInit = (app: Express) => {
    app.use(compression({ filter: shouldCompress }))
}