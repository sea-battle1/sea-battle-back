import {Express} from "express";

const cors = require('cors')

export const corsInit = (app: Express) => {
    app.use(cors({ origin:true, credentials:true }))
}

