import {Request, Response} from "express";
import {getUserInfoById} from "../../db/user";
import * as Sentry from "@sentry/node";
import TelegramBot from "node-telegram-bot-api";

// @ts-ignore
export const tgWin = async (
    tgBot: any,
    req: Request<{}, any, any, any, Record<string, any>>,
    res: Response<any, Record<string, any>>
) => {
    try {
        const userId = req.body.id.replace('TG_', '')
        const user = await getUserInfoById(req.body.id)
        if (user) {
            const options: TelegramBot.SetGameScoreOptions = req.body.gameId.split('---').length === 1
                ? {inline_message_id: req.body.gameId}
                : {chat_id: req.body.gameId.split('---')[0], message_id: req.body.gameId.split('---')[1]}

            const scoresChat:TelegramBot.GameHighScore[] = await tgBot.getGameHighScores(userId, options)

            const currentUserScores = scoresChat.find((el) => {
                const id = el.user.id
                return id.toString() === userId.toString()
            })

            const score = currentUserScores?.score || 0
            // @ts-ignore
            const value = +score > +user.mpWin + +user.singleWin ? +score + 1 : +user.mpWin + +user.singleWin

            await tgBot.setGameScore(userId, value, options)
        }
        res.status(200)
        res.end()
    } catch (err) {
        Sentry.captureException(err);
        res.status(500)
        res.end()
    }
}