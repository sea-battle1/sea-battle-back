import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import TelegramBot from "node-telegram-bot-api";
import {AuthTypes} from "../../../types/enums";
import {getUserInfoById} from "../../db/user";

// @ts-ignore
export const getChatResult = async (
    tgBot: TelegramBot,
    req: Request<{}, any, any, any, Record<string, any>>,
    res: Response<any, Record<string, any>>
) => {
    try {
        const userId = req.query.id.replace('TG_', '')
        const user = await getUserInfoById(req.query.id)
        if (user) {
            const options: TelegramBot.GetGameHighScoresOptions = req.query.gameId.split('---').length === 1
                ? {inline_message_id: req.query.gameId}
                : {chat_id: req.query.gameId.split('---')[0], message_id: req.query.gameId.split('---')[1]}

            const scoresChat = await tgBot.getGameHighScores(userId, options)
            if (scoresChat) {
                const promises = scoresChat.map(crObj => tgBot.getUserProfilePhotos(crObj.user.id)
                    .then(res => ({...res, userId:crObj.user.id})))
                const photos = await Promise.all(promises)

                const photosPromises = photos.map(photo => photo.photos[0] && tgBot
                    .getFile(photo.photos[0][0].file_id).then(res => ({...res, userId: photo.userId})))
                    .filter(el => !!el)
                const photosUrls = await Promise.all(photosPromises)

                scoresChat.forEach(scObj => {
                    const currentUserPhotos = photosUrls.find(ph => ph.userId === scObj.user.id)
                    if (currentUserPhotos) {
                        // @ts-ignore
                        scObj.user.photoUrl = `https://api.telegram.org/file/bot${process.env.TG_BOT_TOKEN}/${currentUserPhotos.file_path}`
                    }
                    // @ts-ignore
                    scObj.user.authType = AuthTypes.TG
                })
            }

            res.status(200).json(scoresChat)
            res.end()
        } else {
            res.status(401)
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500)
        res.end()
    }
}
