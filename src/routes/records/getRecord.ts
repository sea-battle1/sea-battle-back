import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {getGameRecord} from "../../db/gameRecord";
import {GameRecordModelI, GameRecordTypes, StrikeType} from "../../../types/gameRecord";
import {FieldType} from "../../../types/enums";

type GetRecordReqQuery = {
    recordId: string,
}

type GameRecordType = {
    gameId: GameRecordModelI['gameId'],
    type: GameRecordModelI['type'],
    creatorPlayerId: GameRecordModelI['creatorPlayerId'],
    secondPlayerId: GameRecordModelI['secondPlayerId'],
    gameStartTime: GameRecordModelI['gameStartTime'],
    gameEndTime: GameRecordModelI['gameEndTime'],
    startCreatorPlayerField: FieldType,
    startSecondPlayerField: FieldType,
    creatorPlayerStrikes: StrikeType[],
    secondPlayerStrikes: StrikeType[],
    isEndTimeout: GameRecordModelI['isEndTimeout'],
    winnerId: GameRecordModelI['winnerId']
}

type GetRecordResBody = GameRecordType | {message: string}

export const getRecordURL = '/getRecord'

export const getRecord = async (req: Request<{}, {},{}, GetRecordReqQuery>, res: Response<GetRecordResBody>) => {
    try {
        const { recordId } = req.query

        const gameRecord = await getGameRecord(recordId)

        if (!gameRecord) {
            res.status(406).json({message: 'Запись игры не найдена'})
            res.end()
        } else {
            res.status(200).json({
                ...gameRecord.dataValues,
                gameStartTime: gameRecord.gameStartTime,
                gameEndTime: gameRecord.gameEndTime,
                secondPlayerStrikes: gameRecord.secondPlayerStrikes,
                creatorPlayerStrikes: gameRecord.creatorPlayerStrikes,
                startCreatorPlayerField: gameRecord.startCreatorPlayerField,
                startSecondPlayerField: gameRecord.startSecondPlayerField,
            })
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
