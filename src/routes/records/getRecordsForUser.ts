import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {getGameRecordsByUserId} from "../../db/gameRecord";
import {GameRecordTypes} from "../../../types/gameRecord";
import {getResultRecord} from "./utils";

type GetRecordForUserReqQuery = {
    userId: string,
}

type GetRecordForUserResBody = {
    records: {
        gameId: string,
        gameStartTime: number,
        durationGame: number,
        type: GameRecordTypes,
        result: string,
        userSteps: number,
        enemySteps: number,
        userIsWinner: boolean
    }[]
} | {message: string}

export const getRecordForUserURL = '/getRecordForUser'

export const getRecordForUser = async (req: Request<{},{},{}, GetRecordForUserReqQuery>, res: Response<GetRecordForUserResBody>) => {
    try {
        const { userId } = req.query

        const records = await getGameRecordsByUserId(userId)

        if (!records) {
            res.status(406).json({message: 'Не удалось получить запись игры'})
            res.end()
        } else {
            res.status(200).json({
                records: records.map(r => ({
                    gameId: r.gameId,
                    gameStartTime: +r.gameStartTime,
                    durationGame: +((new Date(r.gameEndTime).valueOf() - new Date(r.gameStartTime).valueOf())/1000).toFixed(1),
                    type: r.type,
                    result: getResultRecord(r.isEndTimeout, userId === r.winnerId),
                    userSteps: (userId === r.creatorPlayerId ? r.creatorPlayerStrikes : r.secondPlayerStrikes).length,
                    enemySteps: (userId !== r.creatorPlayerId ? r.creatorPlayerStrikes : r.secondPlayerStrikes).length,
                    userIsWinner: r.winnerId === userId
                }))
            })
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
