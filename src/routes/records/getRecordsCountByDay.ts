import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {getGameCountByDay} from "../../db/gameRecord";

type GetRecordsCountByDayReqQuery = {
    dayTime: number,
}

type GetRecordsCountByDayResBody = number | {message: string}

export const getRecordsCountByDayURL = '/getRecordsCountByDay'

export const getRecordsCountByDay = async (req: Request<{},{},{}, GetRecordsCountByDayReqQuery>, res: Response<GetRecordsCountByDayResBody>) => {
    try {
        const { dayTime } = req.query
        // @ts-ignore
        const recordsCount = await getGameCountByDay(dayTime)

        if (!recordsCount) {
            res.status(406).json({message: 'Не удалось получить количество игр'})
            res.end()
        } else {
            res.status(200).json(recordsCount)
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}