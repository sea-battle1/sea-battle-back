export const getResultRecord = (isTimeOut: boolean, userIsWinner: boolean): string => {
    if (isTimeOut && userIsWinner) return 'Победа по таймауту'
    if (isTimeOut && !userIsWinner) return 'Поражение по таймауту'
    if (!isTimeOut && !userIsWinner) return 'Поражение'
    return 'Победа'
}