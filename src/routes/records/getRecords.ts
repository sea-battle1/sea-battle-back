import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {getGameRecords} from "../../db/gameRecord";
import {GameRecordModelI} from "../../../types/gameRecord";

type GetRecordForUserReqQuery = {
    page: number,
}

type GetRecordForUserResBody = {
    records: GameRecordModelI[],
    count: number
} | {message: string}

export const getRecordsURL = '/getRecords'

export const getRecords = async (req: Request<{},{},{}, GetRecordForUserReqQuery>, res: Response<GetRecordForUserResBody>) => {
    try {
        const records = await getGameRecords(req.query)
        if (!records) {
            res.status(406).json({message: 'Не удалось получить записи игры'})
            res.end()
        } else {
            res.status(200).json(records)
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
