import {Socket} from "socket.io";
import * as Sentry from "@sentry/node";
import {redisClient} from "../../../db/redis";

export const leaveWaitingList = async (
    currentPlayerSocket: Socket
) => {
    try {
        const isHaveWaitingGame = await redisClient.checkIsHaveListWaitingStartGame()
        if (isHaveWaitingGame) {
            const gameId = await redisClient.getGameIdFromWaitingList()
            await redisClient.removeGame(gameId)
            currentPlayerSocket.emit('leaveWaitingList', true)
        }
    } catch (err) {
        Sentry.captureException(err);
    }

}
