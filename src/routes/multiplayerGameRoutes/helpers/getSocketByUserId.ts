import {Server} from "socket.io";
import {DefaultEventsMap} from "socket.io/dist/typed-events";

export const getSocketByUserId = async (
    socketsInstance:  Server,
    userId: string
) => {
    const sockets = await socketsInstance.fetchSockets();
    const currentSocket = sockets.find(s => s.data.userId === userId)
    return currentSocket
}
