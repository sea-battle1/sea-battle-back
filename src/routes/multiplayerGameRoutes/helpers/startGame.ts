import {v4} from "uuid";
import {Server, Socket} from "socket.io";
import * as Sentry from "@sentry/node";
import {redisClient} from "../../../db/redis";
import {MPEventsTypes, WS_MP_Message_Types} from "../../../../types/mpTypes";
import {getUserInfoById, increaseMpGamesCount} from "../../../db/user";
import {GameRecordTypes} from "../../../../types/gameRecord";
import {getSocketByUserId} from "./getSocketByUserId";
import {setTimeoutGameOverMultiplayerGame} from "../../../utils/gameUtils/setTimeoutGameOverSingleGame";
import {FieldConstructor} from "../../../utils/gameUtils/FieldConstructor";

export const startGame = async (
    {userId, userField}: WS_MP_Message_Types,
    socketsInstance: Server,
    currentPlayerSocket: Socket
) => {
    try {
        const isHaveWaitingGame = await redisClient.checkIsHaveListWaitingStartGame()
        if (isHaveWaitingGame) {
            const gameId = await redisClient.getGameIdFromWaitingList()
            const gameInstance = await redisClient.getGame(gameId)

            if (gameInstance.creatorPlayerId === userId) {
                await redisClient.setGameInWaitingList(gameId)
                currentPlayerSocket?.emit(JSON.stringify({
                    type: MPEventsTypes.error,
                    message: 'Ваш аккаунт уже находится в очереди с другого устройства'
                }))
                currentPlayerSocket.data.gameId = gameId
            } else {
                const enemyId = gameInstance.creatorPlayerId

                await redisClient.setGame(gameId, {
                    ...gameInstance,
                    secondPlayerId: userId,
                    gameStartTime: Date.now(),
                    gameEndTime: 0,
                    startSecondPlayerField: userField,
                    secondPlayerCurrentField: userField,
                })

                const enemyUser = await getUserInfoById(enemyId)

                const user = await getUserInfoById(userId)

                if (enemyUser) {
                    await increaseMpGamesCount(enemyId)
                }

                if (user) {
                    await increaseMpGamesCount(userId)
                }

                currentPlayerSocket.data.gameId = gameId
                currentPlayerSocket.emit('startGame',{
                    type: MPEventsTypes.startGame,
                    gameId,
                    enemyId,
                    playerField: userField,
                    enemyField: new FieldConstructor(gameInstance.creatorPlayerCurrentField).getReplacedField(),
                    isYouStep: false,
                    enemyData: enemyUser
                })

                const enemySocket = await getSocketByUserId(socketsInstance, enemyId)
                enemySocket?.emit('startGame', {
                    type: MPEventsTypes.startGame,
                    gameId,
                    enemyId: userId,
                    playerField: gameInstance.creatorPlayerCurrentField,
                    enemyField: new FieldConstructor(userField).getReplacedField(),
                    isYouStep: true,
                    enemyData: user
                })

                const cbOnTimeout = () => {
                    enemySocket?.emit('endGame', {
                        type: MPEventsTypes.endGame,
                        gameId,
                        enemyId: userId,
                        playerField: gameInstance.creatorPlayerCurrentField,
                        enemyField: userField,
                        isYouStep: false,
                        result: 2
                    })
                    currentPlayerSocket.emit('endGame', {
                        type: MPEventsTypes.endGame,
                        gameId,
                        enemyId,
                        enemyField: gameInstance.creatorPlayerCurrentField,
                        playerField: userField,
                        isYouStep: false,
                        result: 1
                    })
                }

                setTimeoutGameOverMultiplayerGame(enemyId, userId, {
                    ...gameInstance,
                    secondPlayerId: userId,
                    gameStartTime: Date.now(),
                    gameEndTime: 0,
                    startSecondPlayerField: userField,
                    secondPlayerCurrentField: userField,
                }, cbOnTimeout)
            }
        } else {
            const gameId = v4()
            await redisClient.setGameInWaitingList(gameId)
            await redisClient.setGame(gameId, {
                creatorPlayerId: userId,
                startCreatorPlayerField: userField,
                creatorPlayerCurrentField: userField,
                type: GameRecordTypes.mp,
                gameId: gameId,
                creatorPlayerStrikes: [],
                gameStartTime: 0,
                gameEndTime: 0,
                secondPlayerStrikes: [],
                secondPlayerId: '',
                startSecondPlayerField: [],
                secondPlayerCurrentField: []
            })
            currentPlayerSocket.data.gameId = gameId
        }
    } catch (err) {
        Sentry.captureException(err);
    }

}
