import {Server, Socket} from "socket.io";
import * as Sentry from "@sentry/node";
import {MPEventsTypes, WS_MP_Strike_Types} from "../../../../types/mpTypes";
import {CellValueEnum} from "../../../../types/enums";
import {increaseMpLooseCount, increaseMpWinsCount} from "../../../db/user";
import {redisClient} from "../../../db/redis";
import {createGameRecord} from "../../../db/gameRecord";
import {getSocketByUserId} from "./getSocketByUserId";
import {setTimeoutGameOverMultiplayerGame} from "../../../utils/gameUtils/setTimeoutGameOverSingleGame";
import {FieldConstructor} from "../../../utils/gameUtils/FieldConstructor";

declare var global: typeof globalThis & {
    timeouts: {
        [gameId: string]:  NodeJS.Timeout
    }
}

export const mpStrikeCell = async (
    {gameId, userId, rowIndex, columnIndex, enemyId}: WS_MP_Strike_Types,
    socketsInstance: Server,
    currentPlayerSocket: Socket
) => {
    try {
        clearTimeout(global.timeouts[gameId])
        let gameInstance = await redisClient.getGame(gameId)

        const userIsCreatorGame = userId === gameInstance.creatorPlayerId
        const enemyField = new FieldConstructor(userIsCreatorGame ? gameInstance.secondPlayerCurrentField : gameInstance.creatorPlayerCurrentField);
        const playerField = new FieldConstructor(userIsCreatorGame ? gameInstance.creatorPlayerCurrentField : gameInstance.secondPlayerCurrentField);

        const isSuccessStrike = enemyField.field[rowIndex][columnIndex] === CellValueEnum.ship

        enemyField.updateField(rowIndex, columnIndex)

        const updatedEnemyField = userIsCreatorGame ? {
            secondPlayerCurrentField: enemyField.field
        } : {
            creatorPlayerCurrentField: enemyField.field
        }

        const updatedPlayerStrikes = userIsCreatorGame
            ? {
                creatorPlayerStrikes: [...gameInstance.creatorPlayerStrikes, {
                    row: +rowIndex,
                    column: +columnIndex,
                    timeFromStart: Date.now() - gameInstance.gameStartTime,
                }]
            }
            : {
                secondPlayerStrikes: [...gameInstance.secondPlayerStrikes, {
                    row: +rowIndex,
                    column: +columnIndex,
                    timeFromStart: Date.now() - gameInstance.gameStartTime,
                }]
            }

        await redisClient.setGame(gameId, {
            ...gameInstance,
            ...updatedEnemyField,
            ...updatedPlayerStrikes
        })

        gameInstance = await redisClient.getGame(gameId)

        const enemySocket = await getSocketByUserId(socketsInstance, enemyId)

        if (enemyField.isHaveShip) {
            enemySocket?.emit('strike', {
                type: MPEventsTypes.strike,
                gameId,
                enemyId: userId,
                enemyField: playerField.getReplacedField(),
                playerField: enemyField.field,
                isYouStep: !isSuccessStrike,
            })
            currentPlayerSocket.emit('strike', {
                type: MPEventsTypes.strike,
                gameId,
                enemyId,
                playerField: playerField.field,
                enemyField: enemyField.getReplacedField(),
                isYouStep: isSuccessStrike,
            })
            const cbOnTimeout = () => {
                enemySocket?.emit('endGame', {
                    type: MPEventsTypes.endGame,
                    gameId,
                    enemyId: userId,
                    playerField: enemyField.field,
                    enemyField: playerField.field,
                    isYouStep: false,
                    result: 2
                })
                currentPlayerSocket.emit('endGame', {
                    type: MPEventsTypes.endGame,
                    gameId,
                    enemyId,
                    enemyField: enemyField.field,
                    playerField: playerField.field,
                    isYouStep: false,
                    result: 1
                })
            }

            setTimeoutGameOverMultiplayerGame(
                userIsCreatorGame ? gameInstance.secondPlayerId : gameInstance.creatorPlayerId,
                userIsCreatorGame ? gameInstance.creatorPlayerId : gameInstance.secondPlayerId,
                gameInstance,
                cbOnTimeout
            )
        } else {
            await increaseMpLooseCount(enemyId)
            await increaseMpWinsCount(userId)

            await createGameRecord({
                ...gameInstance,
                isEndTimeout: false,
                winnerId: userId
            })

            enemySocket?.emit('endGame', {
                type: MPEventsTypes.endGame,
                gameId,
                enemyId: userId,
                enemyField: playerField.field,
                playerField: enemyField.field,
                isYouStep: false,
                result: 2
            })
            currentPlayerSocket.emit('endGame', {
                type: MPEventsTypes.endGame,
                gameId,
                enemyId,
                playerField: playerField.field,
                enemyField: enemyField.field,
                isYouStep: false,
                result: 1
            })

            await redisClient.removeGame(gameId)
        }
    } catch (err) {
        Sentry.captureException(err);
        Sentry.captureException({
            userId: userId,
            gameId: gameId,
            rowIndex,
            columnIndex,
            enemyId
        });
    }
}
