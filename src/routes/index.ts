import {Router} from 'express'
import TelegramBot from "node-telegram-bot-api";
import {startSingleGame, startSingleGameURL} from "./singleGameRoutes/startSingleGame";
import {strikeCell, strikeCellURL} from "./singleGameRoutes/strikeCell";
import {login, loginURL} from "./authUserRoutes/login";
import {getUserInfo, getUserInfoURL} from "./authUserRoutes/getUserInfo";
import {placeBet, placeBetURL} from "./betRoutes/placeBet";
import {resultBet, resultBetURL} from "./betRoutes/resultBet";
import {tgWin} from "./tgRoutes/tgWin";
import {getUsersListRoute, getUsersListURL} from "./authUserRoutes/getUsersList";
import {getRecord, getRecordURL} from './records/getRecord'
import {getRecordForUser, getRecordForUserURL} from './records/getRecordsForUser'
import {getChatResult} from "./tgRoutes/getChatResult";
import {getRecordsCountByDay, getRecordsCountByDayURL} from "./records/getRecordsCountByDay";
import {getRecords, getRecordsURL} from "./records/getRecords";
import {checkBearer, checkBearerURL} from "./authUserRoutes/checkBearer";
import {deleteUser, deleteUserURL} from "./authUserRoutes/deleteUser";
import {updateUser, updateUserURL} from "./authUserRoutes/updateUser";

export const getRoutes = (tgBot: TelegramBot | null) => {
    const router = Router()

    router.get(strikeCellURL, strikeCell)
    router.get(getUserInfoURL, getUserInfo)
    router.get(placeBetURL, placeBet)
    router.get(resultBetURL, resultBet)

    router.get(getRecordURL, getRecord)
    router.get(getRecordForUserURL, getRecordForUser)
    router.get(getRecordsCountByDayURL, getRecordsCountByDay)
    router.get(getRecordsURL, getRecords)

    router.get(getUsersListURL, getUsersListRoute)
    router.delete(deleteUserURL, deleteUser)
    router.put(updateUserURL, updateUser)
    router.post(loginURL, login)
    router.post(startSingleGameURL, startSingleGame)
    router.get(checkBearerURL, checkBearer)

    if (tgBot) {
        router.post('/tgWin', (req, res) => tgWin(tgBot, req, res))
        router.get('/getChatResult', (req, res) => getChatResult(tgBot, req, res))
    }

    return router
}
