import {Request, Response} from "express";
// @ts-ignore
import jwt from 'jsonwebtoken';
import * as Sentry from "@sentry/node";
import {getBearerToken, getUserIdFromToken} from "../../utils/authUtils/AuthUtils";
import {getUserInfoById, user} from "../../db/user";

type DeleteUserBody = {
    bearer: string,
    first_name: string,
    last_name: string,
    nick_name: string
}

export const updateUserURL = '/updateUser'

export const updateUser = async (req: Request<{}, {}, {}, DeleteUserBody>, res: Response) => {
    try {
        const bearer = req.query.bearer

        const userId = getUserIdFromToken(bearer)
        if (userId) {
            await user.update({
                first_name: req.query.first_name,
                last_name: req.query.last_name,
                nick_name: req.query.nick_name
            },{where: {userId}})
            const u = await getUserInfoById(userId)
            res.status(200).json({
                userId: u?.userId,
                first_name: u?.first_name,
                last_name: u?.last_name,
                nick_name: u?.nick_name,
                authType: u?.authType,
                bearer: getBearerToken(u?.userId as string, u?.first_name as string, u?.last_name as string, Date.now() + 12960000)
            })
            res.end()
        } else {
            res.status(401).json({message: 'Пользователь не найден'})
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
