import {Request, Response} from "express";
// @ts-ignore
import jwt from 'jsonwebtoken';

import * as Sentry from "@sentry/node";
import {getUserInfoById} from "../../db/user";
import {getBearerToken} from "../../utils/authUtils/AuthUtils";

type GetUserInfoQuery = {
    bearerToken: string
}

export const checkBearerURL = '/checkBearer'

export const checkBearer = async (req: Request<{}, {}, {}, GetUserInfoQuery>, res: Response) => {
    try {
        const { bearerToken } = req.query
        let parseData
        try {
            parseData = jwt.verify(bearerToken, process.env.JWT_SECRET_KEY as string)
            const userInDb = await getUserInfoById(parseData.userId)

            if (userInDb && parseData.sessionExpire > Date.now()) {
                res.status(200).json({userId: userInDb.userId,
                    first_name: userInDb.first_name,
                    last_name: userInDb.last_name,
                    nick_name: userInDb.nick_name,
                    authType: userInDb.authType,
                    bearer: getBearerToken(userInDb.userId, userInDb.first_name, userInDb.last_name, Date.now() + 12960000)})
                res.end()
            } else {
                res.status(401).json({message: 'Пользователь не найден'})
                res.end()
            }
        } catch (err) {
            parseData = null
            res.status(401).json({message: 'Токен не валиден'})
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
