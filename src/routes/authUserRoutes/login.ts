import {Request, Response} from "express";
import {checkSigVK, getVKID} from "../../utils/authUtils/VK_utils";
import {getBearerToken} from "../../utils/authUtils/AuthUtils";
import {createUser, findUserById} from "../../db/user";
import {AuthTypes} from "../../../types/enums";
import {getGoogleId} from "../../utils/authUtils/googleUtils";
import {getTGId} from "../../utils/authUtils/tgUtils";
import * as Sentry from "@sentry/node";

type loginReqBody = {
    authType: AuthTypes,
    "mid": string,
    "sid": string,
    "sig": string,
    "secret": string,
    "expire": number,
    "user": {
        "id": string,
        "domain": string,
        "href": string,
        "first_name": string,
        "last_name": string,
        "nickname": string
    }
}

export const loginURL = '/login'

export const login = async (req: Request<{}, {}, loginReqBody>, res: Response) => {
    try {
        if  (req.body.authType === AuthTypes.VK) {
            const isValidSession = checkSigVK(req.body)
            if (isValidSession) {
                const vkId = getVKID(req.body.mid)
                const userInDb = await findUserById(vkId)
                if (userInDb) {
                    const bearerVK = getBearerToken(vkId,
                        userInDb.first_name,
                        userInDb.last_name,
                        req.body.expire
                    )
                    res.status(200).json({...userInDb, bearer: bearerVK})
                    res.end()
                } else {
                    const createdUser = await createUser({
                        userId: vkId,
                        first_name: req.body.user.first_name,
                        last_name: req.body.user.last_name,
                        nick_name: req.body.user.domain,
                        authType: AuthTypes.VK
                    })
                    const bearerVK = getBearerToken(vkId,
                        req.body.user.first_name,
                        req.body.user.last_name,
                        req.body.expire
                    )
                    res.status(201).json({...createdUser, bearer: bearerVK})
                    res.end()
                }
            } else {
                res.status(401).json({message: 'Невалидные данные авторизации'})
                res.end()
            }
        } else if (req.body.authType === AuthTypes.Google) {
            const googleId = getGoogleId(req.body.user.id)
            const userInDb = await findUserById(googleId)
            if (userInDb) {
                const bearerVK = getBearerToken(googleId,
                    userInDb.first_name,
                    userInDb.last_name,
                    req.body.expire
                )
                res.status(200).json({...userInDb, bearer: bearerVK})
                res.end()
            } else {
                const createdUser = await createUser({
                    userId: googleId,
                    first_name: req.body.user.first_name,
                    last_name: req.body.user.last_name,
                    nick_name: req.body.user.domain,
                    authType: AuthTypes.Google
                })
                const bearerVK = getBearerToken(googleId,
                    req.body.user.first_name,
                    req.body.user.last_name,
                    req.body.expire
                )
                res.status(201).json({...createdUser, bearer: bearerVK})
                res.end()
            }
        } else if (req.body.authType === AuthTypes.TG) {
            const tgId = getTGId(req.body.user.id)
            const userInDb = await findUserById(tgId)
            if (userInDb) {
                const bearerVK = getBearerToken(tgId,
                    userInDb.first_name,
                    userInDb.last_name,
                    72000000
                )
                res.status(200).json({...userInDb, bearer: bearerVK})
                res.end()
            } else {
                const createdUser = await createUser({
                    userId: tgId,
                    first_name: 'none',
                    last_name: 'none',
                    nick_name: tgId,
                    authType: AuthTypes.TG
                })
                const bearerVK = getBearerToken(tgId,
                    'none',
                    'none',
                    Date.now() + 72000000
                )
                res.status(201).json({...createdUser, bearer: bearerVK})
                res.end()
            }
        } else if (req.body.authType === AuthTypes.VKGame) {
            // @ts-ignore
            const vkId = getVKID(req.body.id)
            const userInDb = await findUserById(vkId)
            if (userInDb) {
                const bearerVK = getBearerToken(vkId,
                    userInDb.first_name,
                    userInDb.last_name,
                    Date.now() + 72000000
                )
                res.status(200).json({...userInDb, bearer: bearerVK})
                res.end()
            } else {
                const createdUser = await createUser({
                    userId: vkId,
                    // @ts-ignore
                    first_name: req.body.first_name || 'none',
                    // @ts-ignore
                    last_name: req.body.last_name || 'none',
                    // @ts-ignore
                    nick_name: req.body.nick_name,
                    authType: AuthTypes.VK
                })
                const bearerVK = getBearerToken(vkId,
                    createdUser.first_name,
                    createdUser.last_name,
                    72000000
                )
                res.status(201).json({...createdUser, bearer: bearerVK})
                res.end()
            }
        } else if (req.body.authType === AuthTypes.Apple) {
            // @ts-ignore
            const appleId = `apple_${req.body.id}`
            const userInDb = await findUserById(appleId)
            if (userInDb) {
                const bearerVK = getBearerToken(appleId,
                    userInDb.first_name,
                    userInDb.last_name,
                    Date.now() + 72000000
                )
                res.status(200).json({...userInDb, bearer: bearerVK})
                res.end()
            } else {
                // @ts-ignore
                if (req.body.first_name && req.body.last_name && req.body.nick_name) {
                    const createdUser = await createUser({
                        userId: appleId,
                        // @ts-ignore
                        first_name: req.body.first_name || 'none',
                        // @ts-ignore
                        last_name: req.body.last_name || 'none',
                        // @ts-ignore
                        nick_name: req.body.nick_name,
                        authType: AuthTypes.Apple
                    })
                    const bearerVK = getBearerToken(appleId,
                        createdUser.first_name,
                        createdUser.last_name,
                        72000000
                    )
                    res.status(201).json({...createdUser, bearer: bearerVK})
                    res.end()
                } else {
                    res.status(204)
                    res.end()
                }
            }
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
