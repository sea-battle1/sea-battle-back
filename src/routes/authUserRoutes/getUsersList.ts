import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {col, FindOptions, fn, literal, Op} from "sequelize";
import {getUsersList} from "../../db/user";
import {AuthTypes} from "../../../types/enums";
import sequelize from "../../db/database";

type GetUserInfoQuery = {
    userId: string
    userIds: string
    limit: string
    sort: string
    authType: AuthTypes
}

export const getUsersListURL = '/getUsersList'

export const getUsersListRoute = async (req: Request<{}, {}, any, GetUserInfoQuery>, res: Response) => {
    try {
        const query = req.query

        // @ts-ignore
        const options: FindOptions | undefined = Object.keys(query).length > 0 ? {
            where: {
                ...(query.userIds ? {
                    userId: {
                        [Op.or]: query.userIds.split(',')
                    }
                } : {}),
                ...(query.authType ? {
                    authType: query.authType
                } : {}),
            },
            limit: query.limit ? query.limit : undefined,
            order: query.sort ? sequelize.literal('"singleWin" + "mpWin" DESC') : undefined

        } : undefined
        const users = await getUsersList(options)
        if (users) {
            res.status(200).json(users)
            res.end()
        } else {
            res.status(400).json({message: 'Не удалось найти пользователей'})
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
