import {Request, Response} from "express";
// @ts-ignore
import jwt from 'jsonwebtoken';
import * as Sentry from "@sentry/node";
import {getUserIdFromToken} from "../../utils/authUtils/AuthUtils";
import {user} from "../../db/user";

type DeleteUserBody = {
    bearer: string,
}

export const deleteUserURL = '/deleteUser'

export const deleteUser = async (req: Request<{}, {}, {}, DeleteUserBody>, res: Response) => {
    try {
        const bearer = req.query.bearer

        const userId = getUserIdFromToken(bearer)
        if (userId) {
            await user.destroy({
                where: {
                    userId
                }
            })
            res.status(200).json(true)
            res.end()
        } else {
            res.status(401).json({message: 'Пользователь не найден'})
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
