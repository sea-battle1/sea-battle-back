import {Request, Response} from "express";
import {getUserInfoById} from "../../db/user";
import {authMiddleware} from "../../utils/authUtils/checkAuth";
import * as Sentry from "@sentry/node";

type GetUserInfoQuery = {
    userId: string
}

export const getUserInfoURL = '/getUserInfo'

export const getUserInfo = async (req: Request<{}, {}, {}, GetUserInfoQuery>, res: Response) => {
    try {
        // const isAuth = authMiddleware(req, res)
        // if (isAuth) {
            const userInDb = await getUserInfoById(req.query.userId)
            if (userInDb) {
                res.status(200).json(userInDb)
                res.end()
            } else {
                res.status(400).json({message: 'Пользователь не найден'})
                res.end()
            }
        // }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
