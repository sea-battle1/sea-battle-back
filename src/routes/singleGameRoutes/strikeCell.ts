import {Request, Response} from "express";
import * as Sentry from "@sentry/node";
import {CellValueEnum} from "../../../types/enums";
import {increaseSingleLooseCount, increaseSingleWinsCount} from "../../db/user";
import {getUserIdFromToken} from "../../utils/authUtils/AuthUtils";
import {redisClient} from "../../db/redis";
import {createGameRecord} from "../../db/gameRecord";
import {setTimeoutGameOverSingleGame} from "../../utils/gameUtils/setTimeoutGameOverSingleGame";
import {FieldConstructor} from "../../utils/gameUtils/FieldConstructor";

declare var global: typeof globalThis & {
    timeouts: {
        [gameId: string]:  NodeJS.Timeout
    }
}

type strikeCellQueryTypes = {
    gameId: string,
    row: string,
    col: string,
}

export const strikeCellURL = '/strikeCell'

export const strikeCell = async (req: Request<{}, {}, {}, strikeCellQueryTypes>, res: Response) => {
    try {
        const {gameId, row, col} = req.query

        let gameInstance = await redisClient.getGame(gameId)
        if (!gameInstance || !global.timeouts[gameId]) {
            res.status(200).json({isEndGameOfTimeOut: true})
            res.end()
        } else {
            clearTimeout(global.timeouts[gameId])
            delete global.timeouts[gameId]

            const {creatorPlayerCurrentField, secondPlayerCurrentField} = gameInstance

            const botField = new FieldConstructor(secondPlayerCurrentField)
            const playerField = new FieldConstructor(creatorPlayerCurrentField)

            botField.updateField(+row, +col)

            if (botField.field[+row][+col] !== CellValueEnum.kill) {
                playerField.strikeRandomCell()
            }

            gameInstance = {
                ...gameInstance,
                creatorPlayerCurrentField: playerField.field,
                secondPlayerCurrentField: botField.field,
                creatorPlayerStrikes: [...gameInstance.creatorPlayerStrikes, {
                    row: +row,
                    column: +col,
                    timeFromStart: Date.now() - gameInstance.gameStartTime,
                }],
                ...(secondPlayerCurrentField[+row][+col] !== CellValueEnum.ship ? {
                    secondPlayerStrikes: [...gameInstance.secondPlayerStrikes, {
                        row: playerField.lastStrikeCell.row,
                        column: playerField.lastStrikeCell.col,
                        timeFromStart: Date.now() - gameInstance.gameStartTime + 10,
                    }
                    ]
                } : {})
            }

            await redisClient.setGame(gameId, gameInstance)

            const responseBody: any = {botField: botField.getReplacedField(), playerField: playerField.field}
            const userId = getUserIdFromToken(req.headers.authorization as string)

            if (!botField.isHaveShip) {
                responseBody.isPlayerWin = true
                if (userId) {
                    await increaseSingleWinsCount(userId)
                }
                await createGameRecord({
                    ...gameInstance,
                    isEndTimeout: false,
                    winnerId: gameInstance.creatorPlayerId
                })

                await redisClient.removeGame(gameId)
            } else if (!playerField.isHaveShip) {
                responseBody.isBotWin = true
                responseBody.botField = botField.field
                if (userId) {
                    await increaseSingleLooseCount(userId)
                }
                await createGameRecord({
                    ...gameInstance,
                    isEndTimeout: false,
                    winnerId: 'computer'
                })

                await redisClient.removeGame(gameId)
            } else if (playerField.lastUpdateIsKilled) {
                responseBody.isKillStrike = true
                setTimeoutGameOverSingleGame(gameId, userId, gameInstance)
            } else {
                setTimeoutGameOverSingleGame(gameId, userId, gameInstance)
            }

            res.status(200).json(responseBody)
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
