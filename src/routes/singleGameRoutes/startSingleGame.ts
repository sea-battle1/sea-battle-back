import {Request, Response} from "express";
import {v4} from 'uuid';
import * as Sentry from "@sentry/node";
import {FieldType} from "../../../types/enums";
import {getUserIdFromToken} from "../../utils/authUtils/AuthUtils";
import {increaseSingleGamesCount} from "../../db/user";
import {redisClient} from "../../db/redis";
import {GameRecordTypes} from "../../../types/gameRecord";
import {setTimeoutGameOverSingleGame} from "../../utils/gameUtils/setTimeoutGameOverSingleGame";
import {FieldConstructor} from "../../utils/gameUtils/FieldConstructor";

type StartSingleGameReqBody = {
    userId: string,
    userField: FieldType
}

export const startSingleGameURL = '/startSingleGame'

export const startSingleGame = async (req: Request<{}, {}, StartSingleGameReqBody>, res: Response) => {
    try {
        const userId = getUserIdFromToken(req.headers.authorization as string)
        const botField = new FieldConstructor().getRandomCompletedField()
        const gameId = v4()

        const gameInstance = {
            creatorPlayerCurrentField: req.body.userField,
            secondPlayerCurrentField: botField.field,
            gameId,
            type: GameRecordTypes.single,
            creatorPlayerId: userId,
            secondPlayerId: 'computer',
            gameStartTime: Date.now(),
            gameEndTime: 0,
            startCreatorPlayerField: req.body.userField,
            startSecondPlayerField: botField.field,
            creatorPlayerStrikes: [],
            secondPlayerStrikes: []
        }

        await redisClient.setGame(gameId, gameInstance)

        setTimeoutGameOverSingleGame(gameId, userId, gameInstance)

        if (userId) {
            await increaseSingleGamesCount(userId)
        }

        const body = {
            enemyField: botField.getReplacedField(),
            gameId: gameId
        }

        res.status(200).json(body)
        res.end()
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
