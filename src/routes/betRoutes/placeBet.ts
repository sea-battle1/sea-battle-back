import {Request, Response} from "express";
import {v4} from "uuid";
import {increaseSimulateGamesCount} from "../../db/user";
import {authMiddleware} from "../../utils/authUtils/checkAuth";
import {DB_Client} from "../../db/redis";
import * as Sentry from "@sentry/node";


type PlaceBetQuery = {
    userId: string,
    bet: string
}

export const placeBetURL = '/placeBet'

export const placeBet = async (req: Request<{}, {}, {}, PlaceBetQuery>, res: Response) => {
    try {
        const isAuth = authMiddleware(req, res)
        if (isAuth) {
            const betId = v4()

            DB_Client.setBet(betId, req.query.bet)

            await increaseSimulateGamesCount(req.query.userId)
            res.status(201).json({betId})
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
