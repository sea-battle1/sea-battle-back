import {Request, Response} from "express";
import {
    increaseSimulateLooseCount,
    increaseSimulateWinsCount
} from "../../db/user";
import {authMiddleware} from "../../utils/authUtils/checkAuth";
import {DB_Client} from "../../db/redis";
import * as Sentry from "@sentry/node";

type ResultBetQuery = {
    userId: string,
    result: string,
    betId: string
}

export const resultBetURL = '/resultBet'

export const resultBet = async (req: Request<{}, {}, {}, ResultBetQuery>, res: Response) => {
    try {
        const isAuth = authMiddleware(req, res)
        if (isAuth) {
            const saveBet = DB_Client.getBet(req.query.betId)
            if (saveBet === req.query.result) {
                await increaseSimulateWinsCount(req.query.userId)
            } else {
                await increaseSimulateLooseCount(req.query.userId)
            }
            DB_Client.removeBet(req.query.betId)
            res.status(200)
            res.end()
        }
    } catch (err) {
        Sentry.captureException(err);
        res.status(500).json({message: 'Server error'})
        res.end()
    }
}
