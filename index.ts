import dotenv from 'dotenv';
dotenv.config();

import express, {Request, Response, NextFunction} from 'express'
import sequelize from './src/db/database'
import { Server } from "socket.io"

import {getAppArgs} from "./src/utils";
import {sentryInit} from "./src/utils/appInit/sentryInit";
import {createServer} from "./src/utils/appInit/createHTTPSServer";
import {corsInit} from "./src/utils/appInit/corsInit";
import {compressionResInit} from "./src/utils/appInit/compressionResInit";
import {tgBotInit} from "./src/utils/appInit/tgBotInit";
import {getRoutes} from "./src/routes";
import {createHTTPServer} from "./src/utils/appInit/createHTTPServer";
import * as Sentry from "@sentry/node";
import {webSocketInit} from "./src/utils/appInit/webSocketInit";

declare var global: typeof globalThis & {
    timeouts: {
        [gameId: string]:  NodeJS.Timeout
    }
}

// Получаем параметры запуска
const argv = getAppArgs()
const PORT = argv.port || process.env.PORT_SEA_BATTLE || 3000

const app = express()

// Подключение сентри
sentryInit(app, +PORT)

// Создаем https сервер и подключаем сертификаты если прод
const server = createServer(app)

// @ts-ignore
const io = new Server(server, {
    cors: {
        origin: true,
        // allowedHeaders: ["my-custom-header"],
        credentials: true,
    },
});

// Подключение вебсокетов
webSocketInit(io)

// TODO Подключаем корс, далее сделать для дев режима
corsInit(app)

// Помойму парсинг запросов в json
app.use(express.json());

// Подключение обработчика для сжатия файлов
compressionResInit(app)

// Создаем телеграмм бота
const tgBot = tgBotInit(+PORT)

// Создаем роуты
const routes = getRoutes(tgBot)

// Подключаем статические роуты
app.use(express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));
app.use('/', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));
app.use('/singleGame', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));
app.use('/multiplayerGame', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));
app.use('/simulateGame', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));
app.use('/profile/:userId', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));

// подключаем роуты
app.use(routes)

// Подключаем статические роуты с редиректом на главную
app.use('/*', express.static(process.env.CLIENT_STATIC_SEA_BATTLE as string));

if (+PORT === 3004) {
    app.use(Sentry.Handlers.errorHandler());
    // Общий обработчик ошибок
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        res.statusCode = 500;
        // @ts-ignore
        res.end(res.sentry + "\n");
    });
}


// Создаем http сервер с редиректом на https если прод
const httpServer = createHTTPServer()

async function start() {
    try {
        // @ts-ignore
        global.bets = {}
        // @ts-ignore
        global.waitingList = []
        // @ts-ignore
        global.games = {}

        global.timeouts = {}
        await sequelize.sync({
            logging: false,
        })
        server?.listen(+PORT, '0.0.0.0', () => {
            console.log(`Server has been started on ${PORT}`)
            console.log(`https://sea-battles.ru/api-docs`)
        })
    } catch (err) {
        console.log(err)
        Sentry.captureException(err);
    }
}

start()
